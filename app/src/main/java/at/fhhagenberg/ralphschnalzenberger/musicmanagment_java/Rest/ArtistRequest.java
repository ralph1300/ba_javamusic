package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.App;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Image;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Member;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public class ArtistRequest extends APICalls implements Command {

    private NetworkCallback<Serializable> mCallback;
    private int mArtistID;

    public ArtistRequest(int artistID, NetworkCallback<Serializable> callback) {
        mCallback = callback;
        mArtistID = artistID;
    }

    @Override
    public void execute() {

        String url = BASEURL + "/" + ARTIST + mArtistID + "?" + AUTH_STRING;
        Log.d(Constants.NETWORK_LOG, url);

        RequestQueue queue = Volley.newRequestQueue(App.get().getApplicationContext());
        url = url.replaceAll("[ ]", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArtistRequestDummy result = new Gson().fromJson(response, ArtistRequestDummy.class);
                        if (result.getMembers() != null) {
                            for (Member m : result.getMembers()) {
                                m.setArtistID(result.getId());
                            }
                        }
                        mCallback.callback(new Artist(result.getName(),result.getId(), result.getResource_url(),
                                result.getReleases_url(), result.getProfile(),
                                result.getImages() != null && result.getImages().size() > 0 ? result.getImages().get(0) : null, result.getMembers()));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Constants.NETWORK_ERROR, "Something wrong happened");
                mCallback.callback(null);
            }
        });
        queue.add(stringRequest);
    }

    private class ArtistRequestDummy {
        private String name;
        private int id;
        private String resource_url;
        private String releases_url;
        private String profile;
        private List<Image> images;
        private List<Member> members;

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }

        public String getResource_url() {
            return resource_url;
        }

        public String getReleases_url() {
            return releases_url;
        }

        public String getProfile() {
            return profile;
        }

        public List<Image> getImages() {
            return images;
        }

        public List<Member> getMembers() {
            return members;
        }
    }
}
