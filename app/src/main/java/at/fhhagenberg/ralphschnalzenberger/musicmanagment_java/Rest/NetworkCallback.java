package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

import java.util.List;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public interface NetworkCallback<T> {

    void callback(T elements);
}
