package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 *  Representation of the MemberTable
 */
public class MemberTable  {

    final static String TABLE_MEMBER = "member";
    final static String COLUMN_ID = "id";
    final static String COLUMN_ACTIVE= "active";
    final static String COLUMN_ARTISTID = "artistID";
    final static String COLUMN_NAME = "name";
    final static String COLUMN_RESOURCEURL = "resourceURL";

    private final static String DATABASE_CREATE = "create table " + TABLE_MEMBER + "(" +
            COLUMN_ID + " integer primary key," +
            COLUMN_ACTIVE + " integer," +
            COLUMN_ARTISTID + " integer," +
            COLUMN_RESOURCEURL + " text," +
            COLUMN_NAME + " text);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
        onCreate(database);
    }

    public static void dropTable(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
    }
}
