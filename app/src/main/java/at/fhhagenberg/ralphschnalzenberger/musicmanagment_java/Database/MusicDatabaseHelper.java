package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 *  Representation of the MusicDataBaseHelper
 */
public class MusicDatabaseHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "music.db";
    private final static int VERSION = 1;

    public MusicDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (db != null) {
            AlbumTable.onCreate(db);
            ArtistTable.onCreate(db);
            MemberTable.onCreate(db);
            SongTable.onCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (db != null) {
            AlbumTable.onUpgrade(db);
            ArtistTable.onUpgrade(db);
            MemberTable.onUpgrade(db);
            SongTable.onUpgrade(db);
        }
    }
    /**
     * Allows to drop all the tables in the database
     * @param db the database
     */
    public void dropDatabase(SQLiteDatabase db) {
        if (db != null) {
            AlbumTable.dropTable(db);
            ArtistTable.dropTable(db);
            SongTable.dropTable(db);
            MemberTable.dropTable(db);
        }
    }
}
