package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.App;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Image;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Member;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Song;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 *  Representation of the MusicDatasoure
 */
public class MusicDataSource {

    private static MusicDataSource mInstance;

    private SQLiteDatabase mDatabase;
    private MusicDatabaseHelper mDatabaseHelper;

    //Singleton
    public static MusicDataSource get() {
        if (mInstance == null) mInstance = getSync();
        return mInstance;
    }

    private static synchronized MusicDataSource getSync() {
        if (mInstance == null) mInstance = new MusicDataSource();
        return mInstance;
    }

    private MusicDataSource() {
        mDatabaseHelper = new MusicDatabaseHelper(App.get().getApplicationContext());
    }

    // Open and close writeabledatabase

    /*
    * Opens the datasource to allow operations
    */
    private void open() {
        try {
            mDatabase = mDatabaseHelper.getWritableDatabase();
            Log.d(Constants.INFO, "Database open");
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            mDatabase = null;
        }
    }
    /**
    Closes the database
     */
    private void close() {
        mDatabaseHelper.close();
        Log.d(Constants.INFO, "Database closed");
        mDatabase = null;
    }

    //load from db
    /**
     * Gets all the albums from the database
     * @return a list of all the albums
    */
    public List<Album> getAllAlbums() {
        ArrayList<Album> albums = new ArrayList<>();

        open();

        if (mDatabase != null) {
            try {
                Cursor cursor = mDatabase.query(
                        AlbumTable.TABLE_ALBUM,
                        new String[]{AlbumTable.COLUMN_ARTISTID, AlbumTable.COLUMN_TITLE,
                                AlbumTable.COLUMN_CATNO, AlbumTable.COLUMN_COUNTRY,
                                AlbumTable.COLUMN_GENRE, AlbumTable.COLUMN_ID,
                                AlbumTable.COLUMN_IMAGE, AlbumTable.COLUMN_RESOURCEURL,
                                AlbumTable.COLUMN_THUMB, AlbumTable.COLUMN_YEAR},
                        null,
                        null,
                        null,
                        null,
                        null
                );
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Album alb = cursorToAlbum(cursor);
                    if (alb != null) {
                        alb.setSongs(loadSongsForAlbumID(alb.getId()));
                        albums.add(alb);
                    }
                    cursor.moveToNext();
                }
                cursor.close();
                return albums;
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            } finally {
                close();
            }
        }
        return null;
    }
    /**
    gets all the artists from the database
    @return a list of all artists
     */
    public List<Artist> getAllArtists() {
        ArrayList<Artist> artists = new ArrayList<>();
        open();
        if (mDatabase != null) {
            try {
                Cursor cursor = mDatabase.query(
                        ArtistTable.TABLE_ARTIST,
                        new String[]{
                                ArtistTable.COLUMN_ID, ArtistTable.COLUMN_PROFILE,
                                ArtistTable.COLUMN_IMAGE, ArtistTable.COLUMN_NAME,
                                ArtistTable.COLUMN_RELEASEURL, ArtistTable.COLUMN_RESURL
                        },
                        null,
                        null,
                        null,
                        null,
                        null
                );
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Artist art = cursorToArtist(cursor);
                    if (art != null) {
                        art.setMembers(loadMembersForArtistID(art.getId()));
                        artists.add(art);
                    }
                    cursor.moveToNext();
                }
                cursor.close();
                return artists;
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            } finally {
                close();
            }
        }
        return null;
    }
    /**
       gets all the Songs from the database
       @param id the id of the album from which the songs are loaded
       @return a list of all Songs
        */
    private List<Song> loadSongsForAlbumID(int id) {
        ArrayList<Song> songs = new ArrayList<>();
        open();
        if (mDatabase != null) {
            try {
                Cursor cursor = mDatabase.query(
                        SongTable.TABLE_SONG,
                        new String[]{
                                SongTable.COLUMN_ID, SongTable.COLUMN_ALBUMID,
                                SongTable.COLUMN_DURATION, SongTable.COLUMN_TITLE
                        },
                        SongTable.COLUMN_ALBUMID + " = ?",
                        new String[] {id + ""},
                        null,
                        null,
                        null
                );
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Song song = cursorToSong(cursor);
                    if (song != null) {
                        songs.add(song);
                    }
                    cursor.moveToNext();
                }
                cursor.close();
                return songs;
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            } finally {
                close();
            }
        }
        return null;
    }

    /**
     * loads all the members from a specific artists
     * @param id the id of the member
     * @return a list of members
     */
    private List<Member> loadMembersForArtistID(int id) {
        ArrayList<Member> members = new ArrayList<>();
        open();
        if (mDatabase != null) {
            try {
                Cursor cursor = mDatabase.query(
                        MemberTable.TABLE_MEMBER,
                        new String[]{
                                MemberTable.COLUMN_ID,
                                MemberTable.COLUMN_ARTISTID, MemberTable.COLUMN_ACTIVE,
                                MemberTable.COLUMN_NAME, MemberTable.COLUMN_RESOURCEURL
                        },
                        MemberTable.COLUMN_ARTISTID + " = ?",
                        new String[] {id + ""},
                        null,
                        null,
                        null
                );
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Member member = cursorToMember(cursor);
                    if (member != null) {
                        members.add(member);
                    }
                    cursor.moveToNext();
                }
                cursor.close();
                return members;
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            } finally {
                close();
            }
        }
        return null;
    }

    //Save data

    /**
     * saves a specific album to the database
     * @param album the album to save to the db
     */
    public void saveAlbum(Album album) {
        ContentValues content = new ContentValues();
        content.put(AlbumTable.COLUMN_ID, album.getId());
        content.put(AlbumTable.COLUMN_IMAGE, imageToString(album.getImage()));
        content.put(AlbumTable.COLUMN_TITLE, album.getTitle());
        content.put(AlbumTable.COLUMN_ARTISTID, album.getArtistID());
        content.put(AlbumTable.COLUMN_CATNO, album.getCatno());
        content.put(AlbumTable.COLUMN_COUNTRY, album.getCountry());
        String genreString = "";
        for (String g : album.getGenre()) {
            genreString += (g + ";");
        }
        content.put(AlbumTable.COLUMN_GENRE, genreString);
        content.put(AlbumTable.COLUMN_RESOURCEURL, album.getResource_url());
        content.put(AlbumTable.COLUMN_THUMB, album.getThumb());
        content.put(AlbumTable.COLUMN_YEAR, album.getYear());

        open();
        try {
            if (mDatabase != null) {
                mDatabase.insertOrThrow(AlbumTable.TABLE_ALBUM, null, content);
                if (album.getSongs() != null) saveSongs(album.getSongs());
            }
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
        } finally {
            close();
        }
    }

    /**
     * saves an artist to the db
     * @param artist the artist to save
     */
    public void saveArtist(Artist artist) {
        ContentValues content = new ContentValues();
        content.put(ArtistTable.COLUMN_ID, artist.getId());
        content.put(ArtistTable.COLUMN_IMAGE, imageToString(artist.getImage()));
        content.put(ArtistTable.COLUMN_NAME, artist.getName());
        content.put(ArtistTable.COLUMN_PROFILE, artist.getProfile());
        content.put(ArtistTable.COLUMN_RESURL, artist.getResource_url());
        content.put(ArtistTable.COLUMN_RELEASEURL, artist.getReleases_url());
        open();
        try {
            if (mDatabase != null) {
                mDatabase.insertOrThrow(ArtistTable.TABLE_ARTIST, null, content);
                if (artist.getMembers() != null) saveMembers(artist.getMembers());
            }
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
        } finally {
            close();
        }
    }

    /**
     * saves a list of songs in the db
     * @param songs the list of songs to save
     */
    private void saveSongs(List<Song> songs) {
        open();
        for (Song s : songs) {
            ContentValues content = new ContentValues();
            content.put(SongTable.COLUMN_ALBUMID, s.getAlbumID());
            content.put(SongTable.COLUMN_DURATION, s.getDuration());
            content.put(SongTable.COLUMN_TITLE, s.getTitle());
            try {
                if (mDatabase != null) {
                    mDatabase.insertOrThrow(SongTable.TABLE_SONG, null, content);
                }
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            }
        }
        close();
    }

    /**
     * saves a list of members in the db
     * @param members the list of members to save
     */
    private void saveMembers(List<Member> members) {
        open();
        for (Member m : members) {
            ContentValues content = new ContentValues();
            content.put(MemberTable.COLUMN_ID, m.getId());
            content.put(MemberTable.COLUMN_ACTIVE, m.getActive() ? 1 : 0);
            content.put(MemberTable.COLUMN_ARTISTID, m.getArtistID());
            content.put(MemberTable.COLUMN_NAME, m.getName());
            content.put(MemberTable.COLUMN_RESOURCEURL, m.getResURL());
            try {
                if (mDatabase != null) {
                    mDatabase.insertOrThrow(MemberTable.TABLE_MEMBER, null, content);
                }
            } catch (Exception ex) {
                Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            }
        }
        close();
    }

    //delete entries

    /**
     * deletes an album and its songs
     * @param id the id of a specific album to delete
     */
    public void deleteAlbumWithID(int id) {
        open();
        if(mDatabase != null) {
            mDatabase.delete(AlbumTable.TABLE_ALBUM, AlbumTable.COLUMN_ID + " = ?", new String[]{id + ""});
            mDatabase.delete(SongTable.TABLE_SONG, SongTable.COLUMN_ALBUMID + " = ?", new String[]{id + ""});
        }
        close();
    }

    /**
     * deletes an artist with a specific id, deletes corresponding members also
     * @param id the id of the artist to delete
     */
    public void deleteArtistWithID(int id) {
        open();
        if(mDatabase != null) {
            mDatabase.delete(ArtistTable.TABLE_ARTIST, ArtistTable.COLUMN_ID + " = ?", new String[]{id + ""});
            mDatabase.delete(MemberTable.TABLE_MEMBER, MemberTable.COLUMN_ARTISTID + " = ?", new String[]{id + ""});
        }
        close();
    }

    //Cursor handler

    /**
     * Transforms a cursor into an album object
     * @param cursor the cursor to transform to an album
     * @return an album
     */
    @Nullable
    private Album cursorToAlbum(Cursor cursor) {
        try {
            return new Album(
                    stringToImage(cursor.getString(6)),
                    cursor.getInt(0),
                    null,
                    cursor.getInt(5),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(9),
                    cursor.getString(7),
                    cursor.getString(3),
                    Arrays.asList(cursor.getString(4).split(";")),
                    cursor.getString(8)
            );
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            return null;
        }
    }

    /**
     * Transforms a cursor into an artist
     * @param cursor the cursor to transform to an artist
     * @return an artist
     */
    @Nullable
    private Artist cursorToArtist(Cursor cursor) {
        try {
            return new Artist(
                    cursor.getString(3),
                    cursor.getInt(0),
                    cursor.getString(5),
                    cursor.getString(4),
                    cursor.getString(1),
                    stringToImage(cursor.getString(2)),
                    null
            );
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            return null;
        }
    }

    /**
     * Transforms a cursor into a song
     * @param cursor the cursor to transform into a song
     * @return a song
     */
    @Nullable
    private Song cursorToSong(Cursor cursor) {
        try {
            return new Song(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(3),
                    cursor.getString(2)
            );
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            return null;
        }
    }

    /**
     * Transforms a cursor into a member
     * @param cursor the cursor to transform
     * @return a member
     */
    @Nullable
    private Member cursorToMember(Cursor cursor) {
        try {
            return new Member(
                    cursor.getInt(2) == 1,
                    cursor.getInt(1),
                    cursor.getInt(0),
                    cursor.getString(3),
                    cursor.getString(4)
            );
        } catch (Exception ex) {
            Log.d(Constants.DATABASE_ERROR, ex.getMessage());
            return null;
        }
    }

    //drop tables

    /**
     * drops all the tables
     */
    public void dropAllTables() {
        open();
        mDatabaseHelper.dropDatabase(mDatabase);
        close();
    }

    /**
     * uses a string to convert it into an image object
     * @param imageString the string to convert
     * @return an image object
     */
    private Image stringToImage(String imageString) {
        if(imageString != null) {
            String[] parts = imageString.split(";");
            return new Image(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), parts[2], parts[3]);
        }
        return null;
    }

    /**
     * uses an image object and converts it into a string
     * @param image image that is converted
     * @return a string
     */
    private String imageToString(Image image) {
            if(image != null) {
                return image.getHeight() + ";" + image.getWidth() + ";" +
                        image.getResource_url() + ";" + image.getUri();
            }
        return null;
    }
}
