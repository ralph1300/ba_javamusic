package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.github.clans.fab.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database.MusicDataSource;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model.AlbumArtistAdapter;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public class AlbumFragment extends BaseFragment {

    private List<Album> mAlbums = new ArrayList<>();

    //UI
    FloatingActionButton searchWithQR;
    FloatingActionButton searchWithText;
    RecyclerView albumList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        searchWithQR = (FloatingActionButton) getActivity().findViewById(R.id.searchWithQRAlbum);
        searchWithText = (FloatingActionButton) getActivity().findViewById(R.id.searchWithTextAlbum);
        albumList = (RecyclerView) getActivity().findViewById(R.id.albumList);

        addOnClickListener(searchWithQR, Constants.ADD_ALBUM_BY_SCANNING_QR);
        addOnClickListener(searchWithText, Constants.ADD_ALBUM_BY_ENTERING_TEXT);

        albumList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        albumList.setLayoutManager(llm);
        showList();
    }

    @Override
    public void onResume() {
        super.onResume();
        MusicDataSource dataSource = MusicDataSource.get();
        if(dataSource != null) {
            mAlbums = dataSource.getAllAlbums();
            showList();
        }
    }

    private void showList() {
        if (mAlbums != null) {
            albumList.setAdapter(new AlbumArtistAdapter<>(mAlbums));
        }
    }
}
