package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

/**
 * contains all of the needed network data
 */
public class APICalls {

    private final static String CONSUMER_SECRET = "McwJlMcvnnmJZLEbZgQjNYPeJSJhfIVe";
    private final static String CONSUMER_KEY = "MDODGpnRyzlNiMEuIkXQ";

    //URLs
    final static String BASEURL = "https://api.discogs.com";
    final static String SEARCH = "database/search";
    final static String ALBUM = "release_title";
    final static String BARCODE = "barcode";
    final static String RELEASE = "releases/";
    final static String ARTIST = "artists/";


    //Authentication String
    final static String AUTH_STRING = "&key=" + CONSUMER_KEY + "&secret=" + CONSUMER_SECRET;

}
