package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.app.Application;

/**
 * Created by ralphschnalzenberger on 03/12/2016.
 */

/**
 * class used to get the context needed in various locations
 */
public class App extends Application {

    private static App instance;
    public static App get() { return instance; }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
