package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.App;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.DetailActivity;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.ImageLoader;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;

/**
 * Created by ralphschnalzenberger on 10/12/2016.
 */

public class AlbumArtistHolder<T> extends RecyclerView.ViewHolder {

    public AlbumArtistHolder(View itemView) {
        super(itemView);
    }

    /**
     * Binds an element to an cardview
     * @param element element, either album or artist
     */
    public void bindElement(T element) {

        TextView nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
        TextView infoTextView = (TextView) itemView.findViewById(R.id.infoTextView);
        TextView inf2TextView = (TextView) itemView.findViewById(R.id.info2TextView);
        ImageView imageView   = (ImageView) itemView.findViewById(R.id.imageView);

        if (element instanceof Album) {
            Album albumToShow = (Album) element;
            if (albumToShow.getImage().getResource_url() != null) ImageLoader.loadImage(albumToShow.getImage().getResource_url(), imageView);
            nameTextView.setText(albumToShow.getTitle());
            infoTextView.setText(albumToShow.getCountry());
            inf2TextView.setText(albumToShow.getCatno());
            itemView.setTag(albumToShow);
        } else if (element instanceof Artist) {
            Artist artistToShow = (Artist) element;
            if (artistToShow.getImage().getResource_url() != null) ImageLoader.loadImage(artistToShow.getImage().getResource_url(), imageView);
            nameTextView.setText(artistToShow.getName());
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            String placeHolder = App.get().getApplicationContext().getString(R.string.member) + ": " + (artistToShow.getMembers() != null ? artistToShow.getMembers().size() : "0");
            infoTextView.setText(placeHolder);
            inf2TextView.setVisibility(View.INVISIBLE);
            itemView.setTag(artistToShow);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailActivity.class);
                if (v.getTag() instanceof Album) {
                    intent.putExtra(Constants.DETAIL, (Album) v.getTag());
                } else if( v.getTag() instanceof Artist) {
                    intent.putExtra(Constants.DETAIL, (Artist) v.getTag());
                }
                v.getContext().startActivity(intent);
            }
        });

    }
}
