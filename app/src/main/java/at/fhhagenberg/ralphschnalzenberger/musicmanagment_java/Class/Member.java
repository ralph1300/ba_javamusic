package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class;

import java.io.Serializable;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of a member
 */
public class Member implements Serializable {

    private Boolean active;
    private int id;
    private int artistID;
    private String name;
    private String resURL;

    public Member(Boolean active, int id, int artistID, String name, String resURL) {
        this.active = active;
        this.id = id;
        this.artistID = artistID;
        this.name = name;
        this.resURL = resURL;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArtistID() {
        return artistID;
    }

    public void setArtistID(int artistID) {
        this.artistID = artistID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResURL() {
        return resURL;
    }

    public void setResURL(String resURL) {
        this.resURL = resURL;
    }
}
