package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the AblumTable
 */
public class AlbumTable {

    final static String TABLE_ALBUM = "album";
    final static String COLUMN_ID = "id";
    final static String COLUMN_ARTISTID = "artistID";
    final static String COLUMN_TITLE = "title";
    final static String COLUMN_CATNO = "catno";
    final static String COLUMN_YEAR = "year";
    final static String COLUMN_RESOURCEURL = "resource_url";
    final static String COLUMN_COUNTRY = "country";
    final static String COLUMN_GENRE = "genre";
    final static String COLUMN_IMAGE = "image";
    final static String COLUMN_THUMB = "thumb";

    private final static String DATABASE_CREATE = "create table " + TABLE_ALBUM + "(" +
            COLUMN_ID + " integer primary key," +
            COLUMN_ARTISTID + " integer," +
            COLUMN_TITLE + " text," +
            COLUMN_CATNO + " text," +
            COLUMN_YEAR + " text," +
            COLUMN_RESOURCEURL + " text," +
            COLUMN_IMAGE + " text," +
            COLUMN_COUNTRY + " text," +
            COLUMN_GENRE + " text," +
            COLUMN_THUMB + " text);";


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_ALBUM);
        onCreate(database);
    }

    public static void dropTable(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_ALBUM);
    }
}
