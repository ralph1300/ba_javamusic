package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the ArtistTable
 */
public class ArtistTable {

    final static String TABLE_ARTIST = "artist";
    final static String COLUMN_ID = "id";
    final static String COLUMN_RESURL = "resourceURL";
    final static String COLUMN_NAME = "name";
    final static String COLUMN_RELEASEURL = "releaseURL";
    final static String COLUMN_PROFILE = "profile";
    final static String COLUMN_IMAGE = "image";

    private final static String DATABASE_CREATE = "create table " + TABLE_ARTIST + "(" +
            COLUMN_ID + " integer primary key," +
            COLUMN_RESURL + " text," +
            COLUMN_IMAGE + " text, " +
            COLUMN_RELEASEURL + " text, " +
            COLUMN_PROFILE + " text, " +
            COLUMN_NAME + " text);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIST);
        onCreate(database);
    }

    public static void dropTable(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIST);
    }
}
