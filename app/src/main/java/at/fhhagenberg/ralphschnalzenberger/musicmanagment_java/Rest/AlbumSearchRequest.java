package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.App;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public class AlbumSearchRequest extends APICalls implements Command {

    private NetworkCallback<List<Album>> mCallback;
    private String mTerm;
    private int mType;

    public AlbumSearchRequest(String term, int type, NetworkCallback<List<Album>> callback) {
        mTerm = term;
        mType = type;
        mCallback = callback;
    }

    /**
     * executes the search
     */
    @Override
    public void execute() {
        String url = "";

        switch (mType) {
            case Constants.ALBUM: url = BASEURL + "/" + SEARCH + "?" + ALBUM + "=" + mTerm + AUTH_STRING;
                break;
            case Constants.BARCODE: url = BASEURL + "/" + SEARCH + "?" + BARCODE + "=" + mTerm + AUTH_STRING;
                break;
            default:
                Log.d(Constants.INFO, "SEARCH FAILED");
                mCallback.callback(null);
                break;
        }

        Log.d(Constants.NETWORK_LOG, url);
        RequestQueue queue = Volley.newRequestQueue(App.get().getApplicationContext());
        url = url.replaceAll("[ ]", "%20");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        AlbumRequestList list = new Gson().fromJson(response, AlbumRequestList.class);
                        mCallback.callback(list.getResults());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Constants.NETWORK_ERROR, "Something wrong happened");
                mCallback.callback(null);
            }
        });
        queue.add(stringRequest);
    }

    private class AlbumRequestList {
        List<Album> results;

        public List<Album> getResults() {
            return results;
        }

        public void setResults(List<Album> results) {
            this.results = results;
        }
    }
}
