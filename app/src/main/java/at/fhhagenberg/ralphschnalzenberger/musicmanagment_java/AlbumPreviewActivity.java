package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Song;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database.MusicDataSource;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model.ListAdapter;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest.AlbumRequest;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest.ArtistRequest;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest.NetworkCallback;

public class AlbumPreviewActivity extends AppCompatActivity implements NetworkCallback<Serializable> {

    private Album mAlbumPreview;
    private Album mLoadedAlbum;

    private ImageView imageView;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_preview);

        mAlbumPreview = (Album) getIntent().getSerializableExtra(Constants.ALBUM_PREVIEW);
        setupUI();
        loadSongsForAlbum();
    }

    private void setupUI() {
        setTitle(getString(R.string.choose));

        TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
        TextView infoTextView = (TextView) findViewById(R.id.infoTextView);
        TextView infoSecTextView = (TextView) findViewById(R.id.infoSecTextView);
        imageView = (ImageView) findViewById(R.id.imageView);
        listView = (ListView) findViewById(R.id.songListView);

        titleTextView.setText(mAlbumPreview.getTitle());
        infoTextView.setText(mAlbumPreview.getCountry());
        infoSecTextView.setText(mAlbumPreview.getYear());
    }

    private void loadSongsForAlbum() {
        new AlbumRequest(mAlbumPreview, this).execute();
    }

    private void saveAlbum() {
        if (mLoadedAlbum != null) {
            MusicDataSource dataSource = MusicDataSource.get();
            if (dataSource != null) {
                dataSource.saveAlbum(mLoadedAlbum);
                if(mLoadedAlbum.getArtistID() != -1) {
                    new ArtistRequest(mLoadedAlbum.getArtistID(), this).execute();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveAlbum();
                return true;
            case R.id.action_discard:
                finish();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void callback(Serializable element) {
        if (element instanceof Album) {
            mLoadedAlbum = (Album) element;
            ImageLoader.loadImage(mLoadedAlbum.getImage().getUri(), imageView);
            ListAdapter<Song> adapter = new ListAdapter<>(getApplicationContext(), R.layout.listitem, mAlbumPreview.getSongs());
            listView.setAdapter(adapter);
        } else if(element instanceof Artist) {
            Artist a = (Artist) element;
            MusicDataSource dataSource = MusicDataSource.get();
            if (dataSource != null) dataSource.saveArtist(a);
            startActivity(new Intent(this, StartScreenActivity.class));
        }
    }
}
