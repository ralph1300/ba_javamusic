package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Member;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database.MusicDataSource;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model.ListAdapter;

public class DetailActivity<T> extends AppCompatActivity {

    private Serializable mObj;

    //UI
    private TextView titleTextView;
    private TextView infoTextView;
    private TextView info2TextView;
    private ListView listView;
    private ImageView overviewImageView;
    private TextView tableHeaderTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        titleTextView       = (TextView) findViewById(R.id.titleTextView);
        infoTextView        = (TextView) findViewById(R.id.infoTextView);
        info2TextView       = (TextView) findViewById(R.id.infoSecTextView);
        listView            = (ListView) findViewById(R.id.listView);
        overviewImageView   = (ImageView) findViewById(R.id.overviewImageView);
        tableHeaderTextView = (TextView) findViewById(R.id.tableHeader);
        
        mObj = getIntent().getSerializableExtra(Constants.DETAIL);
        setupUI(mObj);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.deletemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                createDeleteDialog();
                break;
            default:
                return true;
        }
        return true;
    }

    private void setupUI(Serializable mObj) {
        setTitle("Detail");
        if (mObj instanceof Album) {
            tableHeaderTextView.setText(getString(R.string.songs));
            setupUIForAlbum((Album)mObj);
        } else if (mObj instanceof Artist) {
            tableHeaderTextView.setText(getString(R.string.member));
            setupUIForArtist((Artist)mObj);
        }
    }

    /**
     * sets UI up for an artist
     * @param a the artist
     */
    private void setupUIForArtist(Artist a) {
        infoTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
        titleTextView.setText(a.getName());
        infoTextView.setText(a.getProfile());
        info2TextView.setVisibility(View.INVISIBLE);
        ImageLoader.loadImage(a.getImage().getResource_url(), overviewImageView);
        listView.setAdapter(new ListAdapter<>(getApplicationContext(), R.layout.listitem, a.getMembers()));
    }

    /**
     * sets up UI for an album
     * @param a the album
     */
    private void setupUIForAlbum(Album a) {
        titleTextView.setText(a.getTitle());
        infoTextView.setText(a.getCountry());
        info2TextView.setText(a.getYear());
        ImageLoader.loadImage(a.getImage().getResource_url(), overviewImageView);
        listView.setAdapter(new ListAdapter<>(getApplicationContext(), R.layout.listitem, a.getSongs()));
    }

    /**
     * creates an delete dialog
     */
    private void createDeleteDialog() {
        if (mObj != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
            builder.setTitle(mObj instanceof Album ? ((Album)mObj).getTitle() : ((Artist)mObj).getName());

            final TextView input = new TextView(getApplicationContext());
            input.setText(mObj instanceof Album ? getString(R.string.deleteAlbum) : getString(R.string.deleteArtist));
            input.setTextColor(Color.BLACK);
            builder.setView(input);

            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteObject(mObj);
                    finish();
                }
            });
            builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }

    /**
     * delets an object
     * @param obj artist or album to delete
     */
    private void deleteObject(Serializable obj) {
        MusicDataSource dataSource = MusicDataSource.get();
        if (dataSource != null) {
            if (obj instanceof Album) {
                dataSource.deleteAlbumWithID(((Album)obj).getId());
            } else if(obj instanceof Artist) dataSource.deleteArtistWithID(((Artist)obj).getId());
        }
    }

}
