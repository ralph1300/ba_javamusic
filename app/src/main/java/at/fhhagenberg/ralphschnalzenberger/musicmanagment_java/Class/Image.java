package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class;

import java.io.Serializable;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the image
 */
public class Image implements Serializable{

    private int height;
    private int width;
    private String resource_url;
    private String uri;

    public Image(int height, int width, String resource_url, String uri) {
        this.height = height;
        this.width = width;
        this.resource_url = resource_url;
        this.uri = uri;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getResource_url() {
        return resource_url;
    }

    public void setResource_url(String resource_url) {
        this.resource_url = resource_url;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
