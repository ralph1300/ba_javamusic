package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Fragment.AlbumFragment;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Fragment.ArtistFragment;

public class StartScreenActivity extends AppCompatActivity {

    private ArtistFragment mArtistFragment = new ArtistFragment();
    private AlbumFragment mAlbumFragment   = new AlbumFragment();
    private ViewPager mPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        mPager = (ViewPager) findViewById(R.id.pager);
        TabLayout layout = (TabLayout) findViewById(R.id.tabs);
        setupUI();
        setupViewPager();
        //delete database
        //getApplicationContext().deleteDatabase("music.db");
        layout.setupWithViewPager(mPager);
    }

    private void setupViewPager() {
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(mArtistFragment, getString(R.string.Artist));
        adapter.addFragment(mAlbumFragment, getString(R.string.Album));
        mPager.setAdapter(adapter);
    }

    private void setupUI() {
        try {
            getSupportActionBar().setTitle(getString(R.string.titleName));
        } catch (Exception ex) {
            Log.d("Error", ex.getMessage());
        }
    }

    private class ViewPageAdapter extends FragmentPagerAdapter {

        private ArrayList<Fragment> mFragmentList = new ArrayList<>();
        private ArrayList<String> mFragmentTitleList = new ArrayList<>();

        ViewPageAdapter(FragmentManager manager) {
            super(manager);
        }

        public void addFragment(Fragment frag, String title) {
            mFragmentTitleList.add(title);
            mFragmentList.add(frag);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

