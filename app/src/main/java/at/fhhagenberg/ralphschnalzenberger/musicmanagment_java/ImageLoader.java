package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by ralphschnalzenberger on 11/12/2016.
 */

/**
 * an imageloader
 */
public class ImageLoader {
    /**
     * loads an image asynchronously
     * @param url the url of the image
     * @param imageView the imageview that the image is loaded into
     */
    public static void loadImage(String url, ImageView imageView) {
        if (!url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url).fit().placeholder(R.drawable.ic_album_black_48dp).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Log.d("ERROR", "COULD NOT LOAD IMAGE");
                }
            });
        }
    }
}
