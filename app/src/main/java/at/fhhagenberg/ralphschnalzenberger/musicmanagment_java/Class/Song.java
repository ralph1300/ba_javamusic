package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class;

import java.io.Serializable;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the song
 */
public class Song implements Serializable {

    private int id;
    private int albumID;
    private String title;
    private String duration;

    public Song(int id, int albumID, String title, String duration) {
        this.id = id;
        this.albumID = albumID;
        this.title = title;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlbumID() {
        return albumID;
    }

    public void setAlbumID(int albumID) {
        this.albumID = albumID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
