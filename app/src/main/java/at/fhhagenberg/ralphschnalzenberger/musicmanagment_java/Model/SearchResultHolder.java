package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.AlbumPreviewActivity;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.ImageLoader;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;

/**
 * Created by ralphschnalzenberger on 09/12/2016.
 */

public class SearchResultHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Album mAlbum;

    public SearchResultHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    public void bindSearchResultAlbum(Album album) {

        mAlbum = album;
        if(!album.getThumb().isEmpty() && album.getThumb() != null) {
            ImageView imgView = ((ImageView) itemView.findViewById(R.id.album_img));

            ImageLoader.loadImage(album.getThumb(), imgView);
        }
        ((TextView)itemView.findViewById(R.id.searchResultNameView)).setText(album.getTitle());
        ((TextView)itemView.findViewById(R.id.searchResultCatNoView)).setText(album.getCatno());
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), AlbumPreviewActivity.class);
        intent.putExtra(Constants.ALBUM_PREVIEW, mAlbum);
        v.getContext().startActivity(intent);
        //Toast.makeText(v.getContext(), mAlbum.getTitle(), Toast.LENGTH_LONG).show();
    }
}
