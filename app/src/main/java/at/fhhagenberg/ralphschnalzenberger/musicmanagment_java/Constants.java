package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

/**
 * Created by ralphschnalzenberger on 05/12/2016.
 */

/**
 * constants needed for debugging, types and intents
 */
public class Constants {

    //error logs
    public final static String DATABASE_ERROR = "DB";
    public final static String NETWORK_ERROR  = "NETWORK";
    public final static String INFO           = "INFO";
    public final static String NETWORK_LOG    = "NETWORK";

    //Buttontypes
    public final static int ADD_ALBUM_BY_ENTERING_TEXT = 0;
    public final static int ADD_ALBUM_BY_SCANNING_QR   = 1;

    //Searchtypes
    public final static int ALBUM   = 1;
    public final static int BARCODE = 0;

    //Intents
    public final static String SEARCH_RESULTS   = "SEARCHRESULT";
    public final static String GET_BARCODE_SCAN = "BARCODESCAN";
    public final static String ALBUM_PREVIEW    = "ALBUMPREVIEW";
    public final static String DETAIL           = "DETAIL";

    //Other
    public final static int MAX_LIST_LENGTH = 15;

}
