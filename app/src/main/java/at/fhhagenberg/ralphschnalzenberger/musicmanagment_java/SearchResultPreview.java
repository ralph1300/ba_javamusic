package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model.SearchResultAdapter;

public class SearchResultPreview extends AppCompatActivity {

    private ArrayList<Album> mSearchResults;
    private RecyclerView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result_preview);
        mListView = (RecyclerView) findViewById(R.id.searchResultListView);

        setTitle(getString(R.string.choose));

        try {
            mSearchResults = (ArrayList<Album>) getIntent().getSerializableExtra(Constants.SEARCH_RESULTS);
        } catch (Exception ex) {
            Log.d(Constants.INFO, "Casting error");
            finish();
        }

        mListView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setLayoutManager(llm);
        showList();
    }

    private void showList() {
        mListView.setAdapter(new SearchResultAdapter(mSearchResults));
    }

}
