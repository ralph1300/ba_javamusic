package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import com.github.clans.fab.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.BarcodeScannerActivity;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest.AlbumSearchRequest;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest.NetworkCallback;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.SearchResultPreview;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public class BaseFragment extends Fragment implements NetworkCallback<List<Album>> {

    private ProgressDialog mProgressDialog;

    /**
     * adds an onclick listener to a floating action button
     * @param button the button the onclick is added
     * @param type the type of the button (entering text/scan)
     */
    protected void addOnClickListener(FloatingActionButton button, int type) {
        switch (type) {
            case Constants.ADD_ALBUM_BY_ENTERING_TEXT: {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog();
                    }
                });
                break;
            }
            case Constants.ADD_ALBUM_BY_SCANNING_QR: {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), BarcodeScannerActivity.class);
                        startActivityForResult(intent, 1);
                    }
                });
                break;
            }
            default: break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 1) return;
        if (resultCode == Activity.RESULT_OK) {
            String barcode = data.getStringExtra(Constants.GET_BARCODE_SCAN);
            if (barcode != null) executeAPISearch(barcode, Constants.BARCODE);
        }
    }

    /**
     * shows a dialgo
     */
    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.enter_albumname));

        final EditText input = new EditText(getContext());
        input.setHint(getString(R.string.album_hint));
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String albumTitle = input.getText().toString();
                if (albumTitle.equals("")) {
                    Toast.makeText(getContext(),getString(R.string.empty_search_hint),Toast.LENGTH_LONG).show();
                } else {
                    executeAPISearch(albumTitle, Constants.ALBUM);
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * executes the search
     * @param term the term that is searched for
     * @param type the type (barcode or text)
     */
    private void executeAPISearch(String term, int type) {
        if (!(type == Constants.BARCODE || type == Constants.ALBUM)) return;

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle(getString(R.string.loadTitle));
        mProgressDialog.setMessage(getString(R.string.loadMessage));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        AlbumSearchRequest request = new AlbumSearchRequest(term, type, this);
        request.execute();
    }

    /**
     * the callback that is called when a list of albums is returned
     * @param elements the albums returned from the request
     */
    @Override
    public void callback(List<Album> elements) {
        mProgressDialog.cancel();

        if(elements != null) {
            ArrayList<Album> results = new ArrayList<>(elements);
            if (elements.size() > Constants.MAX_LIST_LENGTH) {
                results = new ArrayList<>(elements.subList(0, Constants.MAX_LIST_LENGTH));
            }
            Intent intent = new Intent(getContext(), SearchResultPreview.class);
            intent.putExtra(Constants.SEARCH_RESULTS, results);
            startActivity(intent);

        } else {
            Toast.makeText(getContext(),getString(R.string.ERROR_NETWORKING), Toast.LENGTH_LONG).show();
        }
    }
}
