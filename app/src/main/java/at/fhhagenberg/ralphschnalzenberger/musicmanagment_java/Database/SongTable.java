package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 */

public class SongTable {

    final static String TABLE_SONG = "song";
    final static String COLUMN_ID = "id";
    final static String COLUMN_ALBUMID = "artistID";
    final static String COLUMN_TITLE = "title";
    final static String COLUMN_DURATION = "duration";

    private final static String DATABASE_CREATE = "create table " + TABLE_SONG + "(" +
            COLUMN_ID + " integer primary key autoincrement," +
            COLUMN_ALBUMID + " integer," +
            COLUMN_DURATION + " text, " +
            COLUMN_TITLE + " text);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_SONG);
        onCreate(database);
    }

    public static void dropTable(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_SONG);
    }
}
