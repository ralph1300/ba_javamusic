package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;

/**
 * Created by ralphschnalzenberger on 10/12/2016.
 */

public class AlbumArtistAdapter<T> extends RecyclerView.Adapter<AlbumArtistHolder<T>>{

    private List<T> mElements;

    public AlbumArtistAdapter(List<T> elements) {
        this.mElements = elements;
    }

    @Override
    public AlbumArtistHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        if (parent != null) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_albumartistoverview, parent, false);
            return new AlbumArtistHolder<T>(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(AlbumArtistHolder<T> holder, int position) {
        if (holder != null) holder.bindElement(mElements.get(position));
    }

    @Override
    public int getItemCount() {
        return mElements.size();
    }
}
