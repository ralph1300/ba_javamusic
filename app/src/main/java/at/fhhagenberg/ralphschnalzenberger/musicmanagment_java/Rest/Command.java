package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

/**
 * interface for network communication
 */
public interface Command {
    void execute();
}
