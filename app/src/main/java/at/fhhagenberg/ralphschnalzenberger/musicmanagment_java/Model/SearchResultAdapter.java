package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;

/**
 * Created by ralphschnalzenberger on 09/12/2016.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultHolder> {

    private ArrayList<Album> mAlbumList;

    public SearchResultAdapter(ArrayList<Album> albumList) {
        mAlbumList = albumList;
    }

    @Override
    public SearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(parent != null){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_searchresult_album, parent, false);
            return new SearchResultHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(SearchResultHolder holder, int position) {
        if (holder == null) return;
        holder.bindSearchResultAlbum(mAlbumList.get(position));
    }

    @Override
    public int getItemCount() {
        return mAlbumList.size();
    }
}
