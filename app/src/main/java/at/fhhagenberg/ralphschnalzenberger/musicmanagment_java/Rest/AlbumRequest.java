package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Rest;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.App;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Album;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Artist;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Image;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Song;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;

/**
 * Created by ralphschnalzenberger on 08/12/2016.
 */

public class AlbumRequest extends APICalls implements Command {

    private NetworkCallback<Serializable> mCallback;
    private Album mAlbumPreview;

    public AlbumRequest(Album albumPreview, NetworkCallback<Serializable> callback) {
        mCallback = callback;
        mAlbumPreview = albumPreview;
    }

    /**
     * executes the search
     */
    @Override
    public void execute() {
        String url = BASEURL + "/" + RELEASE + mAlbumPreview.getId() + "?" + AUTH_STRING;
        Log.d(Constants.NETWORK_LOG, url);

        RequestQueue queue = Volley.newRequestQueue(App.get().getApplicationContext());
        url = url.replaceAll("[ ]", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        AlbumInfoRequestList list = new Gson().fromJson(response, AlbumInfoRequestList.class);

                        for (Song s : list.getTracklist()) {
                            s.setAlbumID(mAlbumPreview.getId());
                        }
                        mAlbumPreview.setSongs(list.getTracklist());

                        if (list.getImages().size() > 0) {
                            mAlbumPreview.setImage(list.getImages().get(0));
                        }

                        if (!list.getArtists().isEmpty()) {
                            mAlbumPreview.setArtistID(list.getArtists().get(0).getId());
                        } else {
                            mAlbumPreview.setArtistID(-1);
                        }

                        mCallback.callback(mAlbumPreview);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(Constants.NETWORK_ERROR, "Something wrong happened");
                mCallback.callback(null);
            }
        });
        queue.add(stringRequest);
    }

    private class AlbumInfoRequestList {
        private List<Image> images;
        private List<Song> tracklist;
        private List<Artist> artists;

        public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

        public List<Song> getTracklist() {
            return tracklist;
        }

        public void setTracklist(List<Song> tracklist) {
            this.tracklist = tracklist;
        }

        public List<Artist> getArtists() {
            return artists;
        }

        public void setArtists(List<Artist> artists) {
            this.artists = artists;
        }
    }
}
