package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Model;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Member;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class.Song;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Constants;
import at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.R;


/**
 * Created by ralphschnalzenberger on 09/12/2016.
 */

public class ListAdapter<T> extends ArrayAdapter<T>{

    private List<T> mData;
    private Context mCtx;
    private int mResID;

    public ListAdapter(Context context, int resource, List<T> data) {
        super(context, resource);
        mData = data;
        mCtx = context;
        mResID = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder;

        if (row == null) {

            LayoutInflater inflater = LayoutInflater.from(mCtx);
            row = inflater.inflate(mResID, parent, false);
            holder = new Holder();
            holder.textView = (TextView) row.findViewById(R.id.txtTitle);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }

        if (mData != null) {
            if (mData.get(position) instanceof Member) {
                Member m = (Member) mData.get(position);
                if(holder.textView != null) {
                    holder.textView.setText(m.getName() + " : " + (m.getActive() ? "active" : "inactive"));
                }
            } else if (mData.get(position) instanceof Song) {
                Song s = (Song) mData.get(position);
                if (holder.textView != null) {
                    holder.textView.setText(s.getTitle() + " : " + s.getDuration());
                }
            } else {
                Log.d(Constants.INFO, "Wrong type in adapter");
            }
        }
        return row;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    private class Holder {
        TextView textView;
    }
}
