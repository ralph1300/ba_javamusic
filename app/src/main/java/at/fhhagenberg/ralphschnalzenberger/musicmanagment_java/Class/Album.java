package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the album
 */
public class Album implements Serializable {

    private Image image;
    private int artistID;
    private List<Song> songs;
    private int id;
    private String title;
    private String catno;
    private String year;
    private String resource_url;
    private String country;
    private List<String> genre;
    private String thumb;

    public Album(Image image, int artistID, List<Song> songs, int id, String title, String catno, String year, String resource_url, String country, List<String> genre, String thumb) {
        this.image = image;
        this.artistID = artistID;
        this.songs = songs;
        this.id = id;
        this.title = title;
        this.catno = catno;
        this.year = year;
        this.resource_url = resource_url;
        this.country = country;
        this.genre = genre;
        this.thumb = thumb;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getArtistID() {
        return artistID;
    }

    public void setArtistID(int artistID) {
        this.artistID = artistID;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCatno() {
        return catno;
    }

    public void setCatno(String catno) {
        this.catno = catno;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getResource_url() {
        return resource_url;
    }

    public void setResource_url(String resource_url) {
        this.resource_url = resource_url;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
