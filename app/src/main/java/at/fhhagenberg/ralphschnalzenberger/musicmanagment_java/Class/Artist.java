package at.fhhagenberg.ralphschnalzenberger.musicmanagment_java.Class;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ralphschnalzenberger on 02/12/2016.
 * Representation of the artist
 */
public class Artist implements Serializable {

    private String name;
    private int id;
    private String resource_url;
    private String releases_url;
    private String profile;
    private Image image;
    private List<Member> members;

    public Artist(String name, int id, String resource_url, String releases_url, String profile, Image image, List<Member> members) {
        this.name = name;
        this.id = id;
        this.resource_url = resource_url;
        this.releases_url = releases_url;
        this.profile = profile;
        this.image = image;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResource_url() {
        return resource_url;
    }

    public void setResource_url(String resource_url) {
        this.resource_url = resource_url;
    }

    public String getReleases_url() {
        return releases_url;
    }

    public void setReleases_url(String releases_url) {
        this.releases_url = releases_url;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }
}
